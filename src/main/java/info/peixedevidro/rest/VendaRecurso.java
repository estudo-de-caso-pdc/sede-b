package info.peixedevidro.rest;

import info.peixedevidro.negocio.dto.SedeDto;
import info.peixedevidro.negocio.dto.VendaDto;
import info.peixedevidro.negocio.venda.IServicoVenda;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.List;

@Stateless
@Path("vendas")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class VendaRecurso {

    @EJB(mappedName = IServicoVenda.JNDI_NAME)
    private IServicoVenda servicoVenda;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @SuppressWarnings("unchecked")
    public List<VendaDto> listar() {
        return servicoVenda.listar();
    }

    @GET
    @Path("{id}")
    public VendaDto getItem(@PathParam("id") Long id) {
        return servicoVenda.obter(id);
    }

    @Transactional
    @POST
    public VendaDto salvarVenda(VendaDto vendaDto) {
        vendaDto.setSede(new SedeDto());
        vendaDto.getSede().setId(2L);
        vendaDto.setDia(new Date());
        if (vendaDto.getId() == null) {
            return servicoVenda.incluir(vendaDto);
        } else {
            return servicoVenda.atualizar(vendaDto);
        }
    }

    @DELETE
    @Path("{id}")
    public void apagarVenda(@PathParam("id") Long id) {
        servicoVenda.excluir(id);
    }
}
